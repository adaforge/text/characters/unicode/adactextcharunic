pragma Warnings (Off);
pragma Ada_95;
with System;
with System.Parameters;
with System.Secondary_Stack;
package ada_main is

   gnat_argc : Integer;
   gnat_argv : System.Address;
   gnat_envp : System.Address;

   pragma Import (C, gnat_argc);
   pragma Import (C, gnat_argv);
   pragma Import (C, gnat_envp);

   gnat_exit_status : Integer;
   pragma Import (C, gnat_exit_status);

   GNAT_Version : constant String :=
                    "GNAT Version: 12.2.0" & ASCII.NUL;
   pragma Export (C, GNAT_Version, "__gnat_version");

   GNAT_Version_Address : constant System.Address := GNAT_Version'Address;
   pragma Export (C, GNAT_Version_Address, "__gnat_version_address");

   Ada_Main_Program_Name : constant String := "_ada_convert" & ASCII.NUL;
   pragma Export (C, Ada_Main_Program_Name, "__gnat_ada_main_program_name");

   procedure adainit;
   pragma Export (C, adainit, "adainit");

   procedure adafinal;
   pragma Export (C, adafinal, "adafinal");

   function main
     (argc : Integer;
      argv : System.Address;
      envp : System.Address)
      return Integer;
   pragma Export (C, main, "main");

   type Version_32 is mod 2 ** 32;
   u00001 : constant Version_32 := 16#081c2778#;
   pragma Export (C, u00001, "convertB");
   u00002 : constant Version_32 := 16#7320ff5f#;
   pragma Export (C, u00002, "system__standard_libraryB");
   u00003 : constant Version_32 := 16#4c09e7c8#;
   pragma Export (C, u00003, "system__standard_libraryS");
   u00004 : constant Version_32 := 16#76789da1#;
   pragma Export (C, u00004, "adaS");
   u00005 : constant Version_32 := 16#57fb1023#;
   pragma Export (C, u00005, "ada__command_lineB");
   u00006 : constant Version_32 := 16#3cdef8c9#;
   pragma Export (C, u00006, "ada__command_lineS");
   u00007 : constant Version_32 := 16#5e074051#;
   pragma Export (C, u00007, "systemS");
   u00008 : constant Version_32 := 16#e12f1eb0#;
   pragma Export (C, u00008, "system__secondary_stackB");
   u00009 : constant Version_32 := 16#85d06901#;
   pragma Export (C, u00009, "system__secondary_stackS");
   u00010 : constant Version_32 := 16#8757dfc5#;
   pragma Export (C, u00010, "ada__exceptionsB");
   u00011 : constant Version_32 := 16#021fcb5c#;
   pragma Export (C, u00011, "ada__exceptionsS");
   u00012 : constant Version_32 := 16#0740df23#;
   pragma Export (C, u00012, "ada__exceptions__last_chance_handlerB");
   u00013 : constant Version_32 := 16#6dc27684#;
   pragma Export (C, u00013, "ada__exceptions__last_chance_handlerS");
   u00014 : constant Version_32 := 16#fd5f5f4c#;
   pragma Export (C, u00014, "system__soft_linksB");
   u00015 : constant Version_32 := 16#16d53d45#;
   pragma Export (C, u00015, "system__soft_linksS");
   u00016 : constant Version_32 := 16#37c92568#;
   pragma Export (C, u00016, "system__soft_links__initializeB");
   u00017 : constant Version_32 := 16#2ed17187#;
   pragma Export (C, u00017, "system__soft_links__initializeS");
   u00018 : constant Version_32 := 16#821dff88#;
   pragma Export (C, u00018, "system__parametersB");
   u00019 : constant Version_32 := 16#8a93e4f7#;
   pragma Export (C, u00019, "system__parametersS");
   u00020 : constant Version_32 := 16#8599b27b#;
   pragma Export (C, u00020, "system__stack_checkingB");
   u00021 : constant Version_32 := 16#38089e5b#;
   pragma Export (C, u00021, "system__stack_checkingS");
   u00022 : constant Version_32 := 16#ec4fa52d#;
   pragma Export (C, u00022, "system__storage_elementsB");
   u00023 : constant Version_32 := 16#33895fa5#;
   pragma Export (C, u00023, "system__storage_elementsS");
   u00024 : constant Version_32 := 16#c71e6c8a#;
   pragma Export (C, u00024, "system__exception_tableB");
   u00025 : constant Version_32 := 16#d32c3648#;
   pragma Export (C, u00025, "system__exception_tableS");
   u00026 : constant Version_32 := 16#6ca2ff63#;
   pragma Export (C, u00026, "system__exceptionsS");
   u00027 : constant Version_32 := 16#69416224#;
   pragma Export (C, u00027, "system__exceptions__machineB");
   u00028 : constant Version_32 := 16#8bdfdbe3#;
   pragma Export (C, u00028, "system__exceptions__machineS");
   u00029 : constant Version_32 := 16#7706238d#;
   pragma Export (C, u00029, "system__exceptions_debugB");
   u00030 : constant Version_32 := 16#6e091802#;
   pragma Export (C, u00030, "system__exceptions_debugS");
   u00031 : constant Version_32 := 16#ee8e331a#;
   pragma Export (C, u00031, "system__img_intS");
   u00032 : constant Version_32 := 16#5c7d9c20#;
   pragma Export (C, u00032, "system__tracebackB");
   u00033 : constant Version_32 := 16#d89db4ec#;
   pragma Export (C, u00033, "system__tracebackS");
   u00034 : constant Version_32 := 16#5f6b6486#;
   pragma Export (C, u00034, "system__traceback_entriesB");
   u00035 : constant Version_32 := 16#961bffdd#;
   pragma Export (C, u00035, "system__traceback_entriesS");
   u00036 : constant Version_32 := 16#a0281f47#;
   pragma Export (C, u00036, "system__traceback__symbolicB");
   u00037 : constant Version_32 := 16#d9e66ad1#;
   pragma Export (C, u00037, "system__traceback__symbolicS");
   u00038 : constant Version_32 := 16#701f9d88#;
   pragma Export (C, u00038, "ada__exceptions__tracebackB");
   u00039 : constant Version_32 := 16#eb07882c#;
   pragma Export (C, u00039, "ada__exceptions__tracebackS");
   u00040 : constant Version_32 := 16#a0d3d22b#;
   pragma Export (C, u00040, "system__address_imageB");
   u00041 : constant Version_32 := 16#ffebdd6b#;
   pragma Export (C, u00041, "system__address_imageS");
   u00042 : constant Version_32 := 16#fd158a37#;
   pragma Export (C, u00042, "system__wch_conB");
   u00043 : constant Version_32 := 16#87046332#;
   pragma Export (C, u00043, "system__wch_conS");
   u00044 : constant Version_32 := 16#5c289972#;
   pragma Export (C, u00044, "system__wch_stwB");
   u00045 : constant Version_32 := 16#aa154f33#;
   pragma Export (C, u00045, "system__wch_stwS");
   u00046 : constant Version_32 := 16#002bec7b#;
   pragma Export (C, u00046, "system__wch_cnvB");
   u00047 : constant Version_32 := 16#81c4a942#;
   pragma Export (C, u00047, "system__wch_cnvS");
   u00048 : constant Version_32 := 16#edec285f#;
   pragma Export (C, u00048, "interfacesS");
   u00049 : constant Version_32 := 16#e538de43#;
   pragma Export (C, u00049, "system__wch_jisB");
   u00050 : constant Version_32 := 16#3473cb68#;
   pragma Export (C, u00050, "system__wch_jisS");
   u00051 : constant Version_32 := 16#179d7d28#;
   pragma Export (C, u00051, "ada__containersS");
   u00052 : constant Version_32 := 16#de53e0a3#;
   pragma Export (C, u00052, "ada__containers__helpersB");
   u00053 : constant Version_32 := 16#229b07a5#;
   pragma Export (C, u00053, "ada__containers__helpersS");
   u00054 : constant Version_32 := 16#86c56e5a#;
   pragma Export (C, u00054, "ada__finalizationS");
   u00055 : constant Version_32 := 16#b4f41810#;
   pragma Export (C, u00055, "ada__streamsB");
   u00056 : constant Version_32 := 16#67e31212#;
   pragma Export (C, u00056, "ada__streamsS");
   u00057 : constant Version_32 := 16#367911c4#;
   pragma Export (C, u00057, "ada__io_exceptionsS");
   u00058 : constant Version_32 := 16#a94883d4#;
   pragma Export (C, u00058, "ada__strings__text_buffersB");
   u00059 : constant Version_32 := 16#bb49bb93#;
   pragma Export (C, u00059, "ada__strings__text_buffersS");
   u00060 : constant Version_32 := 16#e6d4fa36#;
   pragma Export (C, u00060, "ada__stringsS");
   u00061 : constant Version_32 := 16#7e7d940a#;
   pragma Export (C, u00061, "ada__strings__utf_encodingB");
   u00062 : constant Version_32 := 16#84aa91b0#;
   pragma Export (C, u00062, "ada__strings__utf_encodingS");
   u00063 : constant Version_32 := 16#d1d1ed0b#;
   pragma Export (C, u00063, "ada__strings__utf_encoding__wide_stringsB");
   u00064 : constant Version_32 := 16#a373d741#;
   pragma Export (C, u00064, "ada__strings__utf_encoding__wide_stringsS");
   u00065 : constant Version_32 := 16#c2b98963#;
   pragma Export (C, u00065, "ada__strings__utf_encoding__wide_wide_stringsB");
   u00066 : constant Version_32 := 16#22a4a396#;
   pragma Export (C, u00066, "ada__strings__utf_encoding__wide_wide_stringsS");
   u00067 : constant Version_32 := 16#c3fbe91b#;
   pragma Export (C, u00067, "ada__tagsB");
   u00068 : constant Version_32 := 16#8bc79dfc#;
   pragma Export (C, u00068, "ada__tagsS");
   u00069 : constant Version_32 := 16#3548d972#;
   pragma Export (C, u00069, "system__htableB");
   u00070 : constant Version_32 := 16#dfde18ba#;
   pragma Export (C, u00070, "system__htableS");
   u00071 : constant Version_32 := 16#1f1abe38#;
   pragma Export (C, u00071, "system__string_hashB");
   u00072 : constant Version_32 := 16#789b98c5#;
   pragma Export (C, u00072, "system__string_hashS");
   u00073 : constant Version_32 := 16#a42d3f08#;
   pragma Export (C, u00073, "system__unsigned_typesS");
   u00074 : constant Version_32 := 16#856c5db1#;
   pragma Export (C, u00074, "system__val_lluS");
   u00075 : constant Version_32 := 16#273bd629#;
   pragma Export (C, u00075, "system__val_utilB");
   u00076 : constant Version_32 := 16#dc19d6f7#;
   pragma Export (C, u00076, "system__val_utilS");
   u00077 : constant Version_32 := 16#b98923bf#;
   pragma Export (C, u00077, "system__case_utilB");
   u00078 : constant Version_32 := 16#91149704#;
   pragma Export (C, u00078, "system__case_utilS");
   u00079 : constant Version_32 := 16#5fc04ee2#;
   pragma Export (C, u00079, "system__put_imagesB");
   u00080 : constant Version_32 := 16#f204fbed#;
   pragma Export (C, u00080, "system__put_imagesS");
   u00081 : constant Version_32 := 16#22b9eb9f#;
   pragma Export (C, u00081, "ada__strings__text_buffers__utilsB");
   u00082 : constant Version_32 := 16#608bd105#;
   pragma Export (C, u00082, "ada__strings__text_buffers__utilsS");
   u00083 : constant Version_32 := 16#95817ed8#;
   pragma Export (C, u00083, "system__finalization_rootB");
   u00084 : constant Version_32 := 16#11f533c1#;
   pragma Export (C, u00084, "system__finalization_rootS");
   u00085 : constant Version_32 := 16#a8ed4e2b#;
   pragma Export (C, u00085, "system__atomic_countersB");
   u00086 : constant Version_32 := 16#821faf92#;
   pragma Export (C, u00086, "system__atomic_countersS");
   u00087 : constant Version_32 := 16#29cc6115#;
   pragma Export (C, u00087, "system__atomic_primitivesB");
   u00088 : constant Version_32 := 16#6790db1b#;
   pragma Export (C, u00088, "system__atomic_primitivesS");
   u00089 : constant Version_32 := 16#7f1e3740#;
   pragma Export (C, u00089, "interfaces__cB");
   u00090 : constant Version_32 := 16#1bfc3385#;
   pragma Export (C, u00090, "interfaces__cS");
   u00091 : constant Version_32 := 16#7d1b5cb8#;
   pragma Export (C, u00091, "ada__strings__boundedB");
   u00092 : constant Version_32 := 16#ec223ab4#;
   pragma Export (C, u00092, "ada__strings__boundedS");
   u00093 : constant Version_32 := 16#6424b9ce#;
   pragma Export (C, u00093, "ada__strings__mapsB");
   u00094 : constant Version_32 := 16#5349602c#;
   pragma Export (C, u00094, "ada__strings__mapsS");
   u00095 : constant Version_32 := 16#96b40646#;
   pragma Export (C, u00095, "system__bit_opsB");
   u00096 : constant Version_32 := 16#93f4ec6d#;
   pragma Export (C, u00096, "system__bit_opsS");
   u00097 : constant Version_32 := 16#5b4659fa#;
   pragma Export (C, u00097, "ada__charactersS");
   u00098 : constant Version_32 := 16#cde9ea2d#;
   pragma Export (C, u00098, "ada__characters__latin_1S");
   u00099 : constant Version_32 := 16#a6abe208#;
   pragma Export (C, u00099, "ada__strings__searchB");
   u00100 : constant Version_32 := 16#dcefc5ee#;
   pragma Export (C, u00100, "ada__strings__searchS");
   u00101 : constant Version_32 := 16#3498f28c#;
   pragma Export (C, u00101, "ada__strings__superboundedB");
   u00102 : constant Version_32 := 16#db4cf09e#;
   pragma Export (C, u00102, "ada__strings__superboundedS");
   u00103 : constant Version_32 := 16#190570e0#;
   pragma Export (C, u00103, "system__compare_array_unsigned_8B");
   u00104 : constant Version_32 := 16#cee1de32#;
   pragma Export (C, u00104, "system__compare_array_unsigned_8S");
   u00105 : constant Version_32 := 16#74e358eb#;
   pragma Export (C, u00105, "system__address_operationsB");
   u00106 : constant Version_32 := 16#20336cf1#;
   pragma Export (C, u00106, "system__address_operationsS");
   u00107 : constant Version_32 := 16#091deb62#;
   pragma Export (C, u00107, "ada__strings__fixedB");
   u00108 : constant Version_32 := 16#780cfb01#;
   pragma Export (C, u00108, "ada__strings__fixedS");
   u00109 : constant Version_32 := 16#88fa2db0#;
   pragma Export (C, u00109, "ada__strings__maps__constantsS");
   u00110 : constant Version_32 := 16#e56aa583#;
   pragma Export (C, u00110, "ada__text_ioB");
   u00111 : constant Version_32 := 16#a21a351b#;
   pragma Export (C, u00111, "ada__text_ioS");
   u00112 : constant Version_32 := 16#73d2d764#;
   pragma Export (C, u00112, "interfaces__c_streamsB");
   u00113 : constant Version_32 := 16#82d73129#;
   pragma Export (C, u00113, "interfaces__c_streamsS");
   u00114 : constant Version_32 := 16#0095760f#;
   pragma Export (C, u00114, "system__crtlS");
   u00115 : constant Version_32 := 16#1aa716c1#;
   pragma Export (C, u00115, "system__file_ioB");
   u00116 : constant Version_32 := 16#22a58504#;
   pragma Export (C, u00116, "system__file_ioS");
   u00117 : constant Version_32 := 16#3d77d417#;
   pragma Export (C, u00117, "system__os_libB");
   u00118 : constant Version_32 := 16#a46b900e#;
   pragma Export (C, u00118, "system__os_libS");
   u00119 : constant Version_32 := 16#6e5d049a#;
   pragma Export (C, u00119, "system__atomic_operations__test_and_setB");
   u00120 : constant Version_32 := 16#57acee8e#;
   pragma Export (C, u00120, "system__atomic_operations__test_and_setS");
   u00121 : constant Version_32 := 16#99643a74#;
   pragma Export (C, u00121, "system__atomic_operationsS");
   u00122 : constant Version_32 := 16#256dbbe5#;
   pragma Export (C, u00122, "system__stringsB");
   u00123 : constant Version_32 := 16#c5854049#;
   pragma Export (C, u00123, "system__stringsS");
   u00124 : constant Version_32 := 16#fcf6b740#;
   pragma Export (C, u00124, "system__file_control_blockS");
   u00125 : constant Version_32 := 16#44bc8f6a#;
   pragma Export (C, u00125, "ada__text_io__generic_auxB");
   u00126 : constant Version_32 := 16#38ee6479#;
   pragma Export (C, u00126, "ada__text_io__generic_auxS");
   u00127 : constant Version_32 := 16#2fb34529#;
   pragma Export (C, u00127, "system__assertionsB");
   u00128 : constant Version_32 := 16#78043fca#;
   pragma Export (C, u00128, "system__assertionsS");
   u00129 : constant Version_32 := 16#8b2c6428#;
   pragma Export (C, u00129, "ada__assertionsB");
   u00130 : constant Version_32 := 16#cc3ec2fd#;
   pragma Export (C, u00130, "ada__assertionsS");
   u00131 : constant Version_32 := 16#4aa988d0#;
   pragma Export (C, u00131, "system__concat_2B");
   u00132 : constant Version_32 := 16#63414553#;
   pragma Export (C, u00132, "system__concat_2S");
   u00133 : constant Version_32 := 16#f3aa0da6#;
   pragma Export (C, u00133, "system__concat_3B");
   u00134 : constant Version_32 := 16#ce7fa831#;
   pragma Export (C, u00134, "system__concat_3S");
   u00135 : constant Version_32 := 16#3168c2bc#;
   pragma Export (C, u00135, "system__concat_4B");
   u00136 : constant Version_32 := 16#8c9addfd#;
   pragma Export (C, u00136, "system__concat_4S");
   u00137 : constant Version_32 := 16#ee52fa88#;
   pragma Export (C, u00137, "system__finalization_mastersB");
   u00138 : constant Version_32 := 16#d8547522#;
   pragma Export (C, u00138, "system__finalization_mastersS");
   u00139 : constant Version_32 := 16#20ec7aa3#;
   pragma Export (C, u00139, "system__ioB");
   u00140 : constant Version_32 := 16#c045b71e#;
   pragma Export (C, u00140, "system__ioS");
   u00141 : constant Version_32 := 16#35d6ef80#;
   pragma Export (C, u00141, "system__storage_poolsB");
   u00142 : constant Version_32 := 16#653cf216#;
   pragma Export (C, u00142, "system__storage_poolsS");
   u00143 : constant Version_32 := 16#97f0c3af#;
   pragma Export (C, u00143, "system__img_biuS");
   u00144 : constant Version_32 := 16#daae0471#;
   pragma Export (C, u00144, "system__img_llbS");
   u00145 : constant Version_32 := 16#641b1f09#;
   pragma Export (C, u00145, "system__img_lliS");
   u00146 : constant Version_32 := 16#ad5ff103#;
   pragma Export (C, u00146, "system__img_lllbS");
   u00147 : constant Version_32 := 16#0fcb1af1#;
   pragma Export (C, u00147, "system__img_llliS");
   u00148 : constant Version_32 := 16#a72be80f#;
   pragma Export (C, u00148, "system__img_lllwS");
   u00149 : constant Version_32 := 16#869c717a#;
   pragma Export (C, u00149, "system__img_llwS");
   u00150 : constant Version_32 := 16#a82a789d#;
   pragma Export (C, u00150, "system__img_wiuS");
   u00151 : constant Version_32 := 16#7c78c3c5#;
   pragma Export (C, u00151, "system__pool_globalB");
   u00152 : constant Version_32 := 16#4b03ff5c#;
   pragma Export (C, u00152, "system__pool_globalS");
   u00153 : constant Version_32 := 16#1982dcd0#;
   pragma Export (C, u00153, "system__memoryB");
   u00154 : constant Version_32 := 16#05837281#;
   pragma Export (C, u00154, "system__memoryS");
   u00155 : constant Version_32 := 16#7240794d#;
   pragma Export (C, u00155, "system__storage_pools__subpoolsB");
   u00156 : constant Version_32 := 16#6402a21c#;
   pragma Export (C, u00156, "system__storage_pools__subpoolsS");
   u00157 : constant Version_32 := 16#b0df1928#;
   pragma Export (C, u00157, "system__storage_pools__subpools__finalizationB");
   u00158 : constant Version_32 := 16#562129f7#;
   pragma Export (C, u00158, "system__storage_pools__subpools__finalizationS");
   u00159 : constant Version_32 := 16#d50f3cfb#;
   pragma Export (C, u00159, "system__stream_attributesB");
   u00160 : constant Version_32 := 16#4269673d#;
   pragma Export (C, u00160, "system__stream_attributesS");
   u00161 : constant Version_32 := 16#d998b4f3#;
   pragma Export (C, u00161, "system__stream_attributes__xdrB");
   u00162 : constant Version_32 := 16#42985e70#;
   pragma Export (C, u00162, "system__stream_attributes__xdrS");
   u00163 : constant Version_32 := 16#9d359f3d#;
   pragma Export (C, u00163, "system__fat_fltS");
   u00164 : constant Version_32 := 16#bb079630#;
   pragma Export (C, u00164, "system__fat_lfltS");
   u00165 : constant Version_32 := 16#c1d738da#;
   pragma Export (C, u00165, "system__fat_llfS");
   u00166 : constant Version_32 := 16#b2c2bc04#;
   pragma Export (C, u00166, "system__strings__stream_opsB");
   u00167 : constant Version_32 := 16#e156e746#;
   pragma Export (C, u00167, "system__strings__stream_opsS");
   u00168 : constant Version_32 := 16#bcc44f28#;
   pragma Export (C, u00168, "system__val_intS");
   u00169 : constant Version_32 := 16#a7a38f5b#;
   pragma Export (C, u00169, "system__val_unsS");
   u00170 : constant Version_32 := 16#933cf165#;
   pragma Export (C, u00170, "system__val_lliS");
   u00171 : constant Version_32 := 16#cbe53fb9#;
   pragma Export (C, u00171, "system__val_llliS");
   u00172 : constant Version_32 := 16#b3d23c6b#;
   pragma Export (C, u00172, "system__val_llluS");
   u00173 : constant Version_32 := 16#9dc47238#;
   pragma Export (C, u00173, "translatorsB");
   u00174 : constant Version_32 := 16#a0f1024a#;
   pragma Export (C, u00174, "translatorsS");
   u00175 : constant Version_32 := 16#ff6bebee#;
   pragma Export (C, u00175, "ada__containers__hash_tablesS");
   u00176 : constant Version_32 := 16#eab0e571#;
   pragma Export (C, u00176, "ada__containers__prime_numbersB");
   u00177 : constant Version_32 := 16#45c4b2d1#;
   pragma Export (C, u00177, "ada__containers__prime_numbersS");
   u00178 : constant Version_32 := 16#52aa515b#;
   pragma Export (C, u00178, "ada__strings__hashB");
   u00179 : constant Version_32 := 16#1121e1f9#;
   pragma Export (C, u00179, "ada__strings__hashS");
   u00180 : constant Version_32 := 16#bf633851#;
   pragma Export (C, u00180, "translators__aliasB");
   u00181 : constant Version_32 := 16#9287336b#;
   pragma Export (C, u00181, "translators__aliasS");
   u00182 : constant Version_32 := 16#82f8d57a#;
   pragma Export (C, u00182, "translators__blockB");
   u00183 : constant Version_32 := 16#8b3adede#;
   pragma Export (C, u00183, "translators__blockS");

   --  BEGIN ELABORATION ORDER
   --  ada%s
   --  ada.characters%s
   --  ada.characters.latin_1%s
   --  interfaces%s
   --  system%s
   --  system.address_operations%s
   --  system.address_operations%b
   --  system.atomic_operations%s
   --  system.img_int%s
   --  system.img_lli%s
   --  system.img_llli%s
   --  system.io%s
   --  system.io%b
   --  system.parameters%s
   --  system.parameters%b
   --  system.crtl%s
   --  interfaces.c_streams%s
   --  interfaces.c_streams%b
   --  system.storage_elements%s
   --  system.storage_elements%b
   --  system.stack_checking%s
   --  system.stack_checking%b
   --  system.string_hash%s
   --  system.string_hash%b
   --  system.htable%s
   --  system.htable%b
   --  system.strings%s
   --  system.strings%b
   --  system.traceback_entries%s
   --  system.traceback_entries%b
   --  system.unsigned_types%s
   --  system.img_biu%s
   --  system.img_llb%s
   --  system.img_lllb%s
   --  system.img_lllw%s
   --  system.img_llw%s
   --  system.img_wiu%s
   --  system.wch_con%s
   --  system.wch_con%b
   --  system.wch_jis%s
   --  system.wch_jis%b
   --  system.wch_cnv%s
   --  system.wch_cnv%b
   --  system.compare_array_unsigned_8%s
   --  system.compare_array_unsigned_8%b
   --  system.concat_2%s
   --  system.concat_2%b
   --  system.concat_3%s
   --  system.concat_3%b
   --  system.concat_4%s
   --  system.concat_4%b
   --  system.traceback%s
   --  system.traceback%b
   --  system.secondary_stack%s
   --  system.standard_library%s
   --  ada.exceptions%s
   --  system.exceptions_debug%s
   --  system.exceptions_debug%b
   --  system.soft_links%s
   --  system.wch_stw%s
   --  system.wch_stw%b
   --  ada.exceptions.last_chance_handler%s
   --  ada.exceptions.last_chance_handler%b
   --  ada.exceptions.traceback%s
   --  ada.exceptions.traceback%b
   --  system.address_image%s
   --  system.address_image%b
   --  system.exception_table%s
   --  system.exception_table%b
   --  system.exceptions%s
   --  system.exceptions.machine%s
   --  system.exceptions.machine%b
   --  system.memory%s
   --  system.memory%b
   --  system.secondary_stack%b
   --  system.soft_links.initialize%s
   --  system.soft_links.initialize%b
   --  system.soft_links%b
   --  system.standard_library%b
   --  system.traceback.symbolic%s
   --  system.traceback.symbolic%b
   --  ada.exceptions%b
   --  ada.assertions%s
   --  ada.assertions%b
   --  ada.command_line%s
   --  ada.command_line%b
   --  ada.containers%s
   --  ada.containers.prime_numbers%s
   --  ada.containers.prime_numbers%b
   --  ada.io_exceptions%s
   --  ada.strings%s
   --  ada.strings.hash%s
   --  ada.strings.hash%b
   --  ada.strings.utf_encoding%s
   --  ada.strings.utf_encoding%b
   --  ada.strings.utf_encoding.wide_strings%s
   --  ada.strings.utf_encoding.wide_strings%b
   --  ada.strings.utf_encoding.wide_wide_strings%s
   --  ada.strings.utf_encoding.wide_wide_strings%b
   --  interfaces.c%s
   --  interfaces.c%b
   --  system.atomic_primitives%s
   --  system.atomic_primitives%b
   --  system.atomic_counters%s
   --  system.atomic_counters%b
   --  system.atomic_operations.test_and_set%s
   --  system.atomic_operations.test_and_set%b
   --  system.case_util%s
   --  system.case_util%b
   --  system.fat_flt%s
   --  system.fat_lflt%s
   --  system.fat_llf%s
   --  system.os_lib%s
   --  system.os_lib%b
   --  system.val_util%s
   --  system.val_util%b
   --  system.val_lllu%s
   --  system.val_llli%s
   --  system.val_llu%s
   --  ada.tags%s
   --  ada.tags%b
   --  ada.strings.text_buffers%s
   --  ada.strings.text_buffers%b
   --  ada.strings.text_buffers.utils%s
   --  ada.strings.text_buffers.utils%b
   --  system.put_images%s
   --  system.put_images%b
   --  ada.streams%s
   --  ada.streams%b
   --  system.file_control_block%s
   --  system.finalization_root%s
   --  system.finalization_root%b
   --  ada.finalization%s
   --  ada.containers.helpers%s
   --  ada.containers.helpers%b
   --  ada.containers.hash_tables%s
   --  system.file_io%s
   --  system.file_io%b
   --  system.storage_pools%s
   --  system.storage_pools%b
   --  system.finalization_masters%s
   --  system.finalization_masters%b
   --  system.storage_pools.subpools%s
   --  system.storage_pools.subpools.finalization%s
   --  system.storage_pools.subpools.finalization%b
   --  system.storage_pools.subpools%b
   --  system.stream_attributes%s
   --  system.stream_attributes.xdr%s
   --  system.stream_attributes.xdr%b
   --  system.stream_attributes%b
   --  system.val_lli%s
   --  system.val_uns%s
   --  system.val_int%s
   --  ada.text_io%s
   --  ada.text_io%b
   --  ada.text_io.generic_aux%s
   --  ada.text_io.generic_aux%b
   --  system.assertions%s
   --  system.assertions%b
   --  system.bit_ops%s
   --  system.bit_ops%b
   --  ada.strings.maps%s
   --  ada.strings.maps%b
   --  ada.strings.maps.constants%s
   --  ada.strings.search%s
   --  ada.strings.search%b
   --  ada.strings.fixed%s
   --  ada.strings.fixed%b
   --  ada.strings.superbounded%s
   --  ada.strings.superbounded%b
   --  ada.strings.bounded%s
   --  ada.strings.bounded%b
   --  system.pool_global%s
   --  system.pool_global%b
   --  system.strings.stream_ops%s
   --  system.strings.stream_ops%b
   --  translators%s
   --  translators%b
   --  translators.alias%s
   --  translators.alias%b
   --  translators.block%s
   --  translators.block%b
   --  convert%b
   --  END ELABORATION ORDER

end ada_main;
